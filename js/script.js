// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//
//     Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

createNewUser = () => {
    newUser = {};
    newUser.firstName = prompt('Input your name','');
    newUser.lastName = prompt('Input your last name','');
    newUser.birthday = prompt('Input your birthday in format dd.mm.yyyy','');
    newUser.getLogin = function() {
        return this.firstName.toLowerCase().charAt(0) + this.lastName.toLowerCase();
    };
    newUser.getAge = function () {
        let newBirthday = this.birthday.split(".").reverse().join("-"),
            ageMs = Date.now() - Date.parse(newBirthday),
            ageDate = new Date(ageMs);

        return ageDate.getFullYear() - 1970;
    }
    newUser.getPassword = function () {
        return this.firstName.toUpperCase().charAt(0) + this.lastName.toLowerCase() + this.birthday.substr(6,4);
    }
    return newUser;
};

console.log(createNewUser());
console.log(newUser.getAge());
console.log(newUser.getPassword());




